﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Core.Singleton;
using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Request
{
	public class RequestManager : StaticSingleton<RequestManager>
	{
		private ServerConfig _serverConfig;
		private string _url;

		public override void Init()
		{
			if (_serverConfig == null)
			{
				base.Init();
				_serverConfig = Resources.Load<ServerConfig>("Configs/ServerConfigs");
				InitRequestEnvironement();
			}
		}

		private void InitRequestEnvironement()
		{
			ServerConfig.Environment currentEnvironment;
#if UNITY_EDITOR
			int index = EditorPrefs.GetInt("RequestEnvironmentSetTo", -1);
			if (index == -1)
			{
				currentEnvironment = ServerConfig.Environment.Dev;
			}
			else
			{
				currentEnvironment = (ServerConfig.Environment)index;
			}
#elif QA_BUILD
			currentEnvironment = ServerConfig.Environment.Qa;
#elif STAGE_BUILD
			currentEnvironment = ServerConfig.Environment.Stage;
#elif LIVE_BUILD
			currentEnvironment = ServerConfig.Environment.Live;
#else
			currentEnvironment = ServerConfig.Environment.Dev;
#endif

			foreach (var item in _serverConfig.ServerUrls)
			{
				if (currentEnvironment == item.Environment)
				{
					_url = item.URL;
				}
			}

			Core.Debug.Log("Request", string.Format("Running in {0} at {1}.", currentEnvironment, _url));
		}

		public void Request<T>(T request, Action<T> callback) where T : BaseRequest
		{
			string route = string.Format("{0}/{1}", _url, request.Route);
			Core.SimpleRoutine.StartCoroutine(Request(route, request.ObjectToJson(), request.Headers,(success, response) =>
			{
				request.JsonToObject(response);
				callback.SafeInvoke(request);
			}));
		}

		private IEnumerator Request(string URL, string json, Dictionary<string, string> headers, Action<bool, string> callback)
		{
			bool success = false;
			var request = new UnityWebRequest(URL, "POST");
			byte[] bodyRaw = new System.Text.UTF8Encoding().GetBytes(json);
			request.uploadHandler = new UploadHandlerRaw(bodyRaw);
			request.downloadHandler = new DownloadHandlerBuffer();

			foreach(var header in headers)
			{
				request.SetRequestHeader(header.Key, header.Value);
			}

			Core.Debug.Log("Request", string.Format("Sending {1} with {0}", URL, json));

			yield return request.SendWebRequest();

			if (request.isNetworkError || request.isHttpError)
			{
				success = false;
			}
			else
			{
				success = true;
			}

			callback.SafeInvoke(success, request.downloadHandler.text);
		}
	}
}