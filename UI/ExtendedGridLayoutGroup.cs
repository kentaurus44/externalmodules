﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExtendedGridLayoutGroup : GridLayoutGroup
{
    [SerializeField]
    protected bool _isSquare;

    [SerializeField]
    protected bool _keepRatio;

    private float _ratio;

    public bool IsSquare
    {
        get { return _isSquare; }
    }

    public bool KeepRatio
    {
        get { return _keepRatio; }
    }

    protected override void Awake()
    {
        base.Awake();
        UpdateRatio();
        UpdateSpacing();
    }

    [ExecuteInEditMode]
    protected void Update()
    {
#if UNITY_EDITOR
        UpdateRatio();
        UpdateSpacing();
#endif
    }

    private void UpdateRatio()
    {
        switch (constraint)
        {
            case Constraint.Flexible:
                break;
            case Constraint.FixedColumnCount:
                _ratio = cellSize.y / cellSize.x;
                break;
            case Constraint.FixedRowCount:
                _ratio = cellSize.x / cellSize.y;
                break;
            default:
                break;
        }
    }
    
    private void UpdateSpacing()
    {
        Vector2 size = cellSize;
        switch (constraint)
        {
            case Constraint.Flexible:
                break;
            case Constraint.FixedColumnCount:
                size.x = (Screen.width - spacing.x * constraintCount) / constraintCount;
                if (_isSquare)
                {
                    size.y = size.x;
                }

                if (_keepRatio)
                {
                    size.y = size.x * _ratio;
                }
                break;
            case Constraint.FixedRowCount:
                break;
            default:
                break;
        }
        cellSize = size;
    }

}
