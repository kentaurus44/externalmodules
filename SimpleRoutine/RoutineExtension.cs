﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class RoutineExtension
{
	public static void SafeCancel(this Coroutine routine)
	{
		if (routine != null)
		{
			RoutineManager.Instance.StopCoroutine(routine);
		}
	}
}
