﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core
{
	public class SimpleRoutine
	{
		private static MonoBehaviour _monobehaviour;

		public static Coroutine StartCoroutine(IEnumerator routine)
		{
			return RoutineManager.Instance.StartCoroutine(routine);
		}
	}
}