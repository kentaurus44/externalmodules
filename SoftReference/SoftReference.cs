﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(SoftReference))]
public class SoftReferenceDrawer : PropertyDrawer
{
	SerializedObject _property;

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		SoftReferenceAttribute attr = (SoftReferenceAttribute)attribute;
		if (property != null && _property == null)
		{
			_property = property.serializedObject;
		}

		if (_property != null)
		{
			Debug.Log(_property.FindProperty("_id"));
		}
	}
}

public class SoftReferenceAttribute : PropertyAttribute
{
	public System.Type Type { get; private set; }
	public SoftReferenceAttribute(System.Type type)
	{
		Type = type;
	}
}

[System.Serializable]
public class SoftReference : Object
{
	[SerializeField]
	public string _id;
}