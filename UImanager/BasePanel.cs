﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BasePanel : MonoBehaviour
{
    [SerializeField]
    protected bool _isPausing = false;

    public virtual void Init() { }

    public void Activate()
    {
        OnActivated();
    }

    public void Deactivate()
    {
        OnDeactivated();
    }

    protected virtual void OnActivated() {}

    protected virtual void OnDeactivated() {}
}


public class BasePanel<T> : BasePanel where T : BasePanel
{
    public static void Show()
    {
        UIManager.Instance.Show<T>();
    }

    public static void Hide()
    {
        UIManager.Instance.Hide<T>();
    }

    public static T Instance
    {
        get
        {
            return UIManager.Instance.GetPanel<T>() as T;
        }
    }
}
