﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : SingletonComponent<UIManager>
{
    [SerializeField]
    protected BasePanel[] _panels;
    [SerializeField]
    protected Canvas _rootDisplay1;

    protected List<BasePanel> _loadedPanels = new List<BasePanel>();

    public IEnumerator Load()
    {
        BasePanel panel;
        _rootDisplay1.transform.localPosition = Vector3.zero;
        for (int i = 0, count = _panels.Length; i < count; ++i)
        {
            yield return panel = Instantiate(_panels[i], _rootDisplay1.transform, false);
            panel.gameObject.SetActive(false);
            panel.transform.localPosition = Vector3.zero;
            panel.name = _panels[i].name;
            _loadedPanels.Add(panel);
            panel.Init();
            panel = null;
			_panels[i] = null;
		}

        _panels = null;
    }

    public void Show<T>()
    {
        BasePanel panel = FindPanel<T>();
		if (panel == null)
		{
			Debug.LogErrorFormat("{0} was not found.", typeof(T).Name);
			return;
		}
        panel.gameObject.SetActive(true);
        panel.transform.SetAsLastSibling();
        panel.Activate();
    }

    public void Hide<T>()
    {
        BasePanel panel = FindPanel<T>();
        panel.Deactivate();
        panel.gameObject.SetActive(false);
    }
    
    public BasePanel GetPanel<T>()
    {
        return FindPanel<T>();
    }

    private BasePanel FindPanel<T>()
    {
        for (int i = 0, count = _loadedPanels.Count; i < count; ++i)
        {
            if (_loadedPanels[i] is T)
            {
                return _loadedPanels[i];
            }
        }
        return null;
    }
}
