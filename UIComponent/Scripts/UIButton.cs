﻿using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIButton : MonoBehaviour
{
	[SerializeField]
	protected Button _button;

	protected void Start()
	{
		_button.onClick.AddListener(OnButtonCB);
	}

	public void BindCallback(UnityAction callback)
	{
		_button.onClick.AddListener(callback);
	}

	public void UnbindCallback(UnityAction callback)
	{
		_button.onClick.RemoveListener(callback);
	}

	protected virtual void OnButtonCB()
	{
		// STUB
	}
}
