﻿using UnityEngine;
using Core.Flow;
using Core.CustomCamera;

public class GameInit : MonoBehaviour
{
	protected void Start()
	{
		CameraManager.Instance.Init();
		FlowManager.Instance.Launch();
	}
}