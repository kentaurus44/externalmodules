﻿//
// Script name: SingletonComponent
//
//
// Programmer: Kentaurus
//

using UnityEngine;
using System.Collections;

public class SingletonComponent : MonoBehaviour
{ }

public class SingletonComponent<T> : SingletonComponent where T : Component, new()
{
    #region Variables
    protected static T m_Instance;
    protected bool m_IsInitialized = false;

    public static T Instance
    {
        get
        {
            if (IsInstanceNull)
            {
                GameObject obj = new GameObject(typeof(T).Name);
                m_Instance = obj.AddComponent<T>();
            }
            return m_Instance;
        }
    }

    public static bool IsInstanceNull
    {
        get { return m_Instance == null; }
    }
    #endregion

    #region Unity API
	protected virtual void Awake()
	{
		m_Instance = gameObject.GetComponent<T>();
		Init();
	}
    #endregion

    #region Public Methods
    public virtual void Init()
    {
        if (!m_IsInitialized)
        {
            m_IsInitialized = true;
        }
    }
    #endregion

    #region Protected Methods
    #endregion

    #region Private Methods
    #endregion

}
