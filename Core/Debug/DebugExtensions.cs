﻿using System.Text;

namespace Core
{
	public static class Debug
	{
		public static void Log(string tag, object message)
		{
			if (IsTagValid(tag))
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("<color=purple>");
				builder.Append(tag);
				builder.Append(" - </color>");
				builder.Append(message);

				UnityEngine.Debug.Log(builder.ToString());
			}
		}

		public static void LogFormat(string tag, string format, params object[] args)
		{
			if (IsTagValid(tag))
			{
				Log(tag, string.Format(format, args));
			}
		}

		public static void LogWarning(string tag, object message)
		{
			if (IsTagValid(tag))
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("<color=yellow>");
				builder.Append(tag);
				builder.Append(" - </color>");
				builder.Append(message);

				UnityEngine.Debug.LogWarning(builder.ToString());
			}
		}

		public static void LogWarningFormat(string tag, string format, params object[] args)
		{
			if (IsTagValid(tag))
			{
				LogWarningFormat(tag, string.Format(format, args));
			}
		}

		public static void LogError(string tag, object message)
		{
			if (IsTagValid(tag))
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("<color=red>");
				builder.Append(tag);
				builder.Append(" - </color>");
				builder.Append(message);

				UnityEngine.Debug.LogError(builder.ToString());
			}
		}

		public static void LogErrorFormat(string tag, string format, params object[] args)
		{
			if (IsTagValid(tag))
			{
				Log(tag, string.Format(format, args));
			}
		}

		public static bool IsTagValid(string tag)
		{
#if UNITY_EDITOR
			return DebugEditorData.IsValid(tag);
#elif !RELEASE_BUILD
			return true;
#endif
		}
	}
}