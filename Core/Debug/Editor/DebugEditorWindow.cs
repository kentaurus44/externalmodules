﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Core
{
	public class DebugEditorWindow : EditorWindow
	{
		private const string kTitle = "Tag Log Enabler";

		[MenuItem("Kentaurus/ OpenTag Log Enabler")]
		static void Init()
		{
			DebugEditorWindow window = (DebugEditorWindow)GetWindow(typeof(DebugEditorWindow));
			_windowTitle.text = kTitle;
			window.titleContent = _windowTitle;
			window.Show();
		}

		private static Vector2 _scrollPosition;
		private static readonly Color kDefaultColor = Color.white;

		private static readonly Color kActiveColor = Color.black;
		private static readonly Color kInactiveColor = Color.red;

		private GUIStyle _active = new GUIStyle();
		private GUIStyle _inactive = new GUIStyle();
		private GUIStyle _header = new GUIStyle();
		private static GUIContent _windowTitle = new GUIContent();

		private Event _currentEvent;

		private void OnEnable()
		{
			_active.normal.textColor = kActiveColor;
			_inactive.normal.textColor = kInactiveColor;
			_header.alignment = TextAnchor.MiddleCenter;
			_header.fontSize = 20;

		}

		private void OnGUI()
		{
			Header();
			MainView();
			Footer();
		}

		private void Header()
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(kTitle, _header);
			EditorGUILayout.EndHorizontal();
		}

		private void MainView()
		{
			_currentEvent = Event.current;

			_scrollPosition = EditorGUILayout.BeginScrollView(_scrollPosition);

			foreach (var data in DebugEditorData.Data)
			{
				Rect rect = EditorGUILayout.BeginHorizontal();
				bool valid = data.IsValid;
				if (_currentEvent.button == 0 && _currentEvent.isMouse && _currentEvent.type == EventType.MouseDown)
				{
					if (rect.Contains(_currentEvent.mousePosition))
					{
						valid = !valid;
						data.IsValid = valid;
						DebugEditorData.Save(data);
						Repaint();
					}
				}

				EditorGUILayout.LabelField(data.Tag, valid ? _active : _inactive);

				EditorGUILayout.EndHorizontal();
			}
			EditorGUILayout.EndScrollView();
		}

		private void Footer()
		{
			GUI.color = kDefaultColor;

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Enable All"))
			{
				DebugEditorData.ToggleAll(true);
			}

			if (GUILayout.Button("Disable All"))
			{
				DebugEditorData.ToggleAll(false);
			}

			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();

			if (GUILayout.Button("Clear"))
			{
				DebugEditorData.Clear();
			}

			EditorGUILayout.EndHorizontal();
		}
	}
}