﻿#if UNITY_EDITOR
using UnityEditor;
using System.Collections.Generic;
using System.Text;

namespace Core
{
	[InitializeOnLoad]
	public class DebugEditorData
	{
		public class TagData
		{
			public string Tag;
			public bool IsValid;

			public TagData(string tag)
			{
				Tag = tag;
				IsValid = EditorPrefs.GetBool(tag, true);
			}

			public TagData(string tag, bool valid)
			{
				Tag = tag;
				IsValid = valid;
			}
		}

		private const string kDebugConsoleKey = "DEBUG_CONSOLE";
		private const char kSeperatorKey = '_';
		private const string kEditorTag = "Editor";

		public static List<TagData> Data = new List<TagData>();

		static DebugEditorData()
		{
			Load();
		}

		public static bool IsValid(string tag)
		{
			TagData data = Data.Find(x => x.Tag.Equals(tag));

			if (data != null)
			{
				return data.IsValid;
			}
			else if (tag.IsEmptyOrNull())
			{
				return false;
			}
			else
			{
				TagData tagData = new TagData(tag);
				Data.Add(tagData);
				Save();
				return true;
			}
		}

		public static void Save()
		{
			StringBuilder builder = new StringBuilder();

			foreach (var data in Data)
			{
				if (!data.Tag.IsEmptyOrNull())
				{
					builder.Append(data.Tag);

					builder.Append(kSeperatorKey);
					Save(data);
				}
			}

			EditorPrefs.SetString(kDebugConsoleKey, builder.ToString());
			Debug.Log(kEditorTag, "DebugEditorData - All Saved");
		}

		public static void Save(TagData data)
		{
			EditorPrefs.SetBool(data.Tag, data.IsValid);
		}

		public static void Load()
		{
			string key = EditorPrefs.GetString(kDebugConsoleKey);

			string[] keys = key.Split(kSeperatorKey);

			foreach (var boolKeys in keys)
			{
				if (!boolKeys.IsEmptyOrNull())
				{
					IsValid(boolKeys);
				}
			}

			IsValid(kEditorTag);

			Debug.Log(kEditorTag, "DebugEditorData - Loaded");
		}

		public static void ToggleAll(bool enabled)
		{
			foreach(var data in Data)
			{
				data.IsValid = enabled;
			}
			Save();
		}

		public static void Clear()
		{
			foreach (var data in Data)
			{
				if (!data.Tag.IsEmptyOrNull() && !data.Tag.Equals(kEditorTag))
				{
					EditorPrefs.DeleteKey(data.Tag);
				}
			}

			EditorPrefs.SetString(kDebugConsoleKey, kEditorTag);

			Data.Clear();
			Load();
			Debug.Log(kEditorTag, "DebugEditorData - Cleared");
		}
	}
}
#endif