﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public static class SystemExtensions 
{
    public static void SafeInvoke(this Action action)
    {
        if (action != null)
        {
            action();
        }
    }

    public static void SafeInvoke<T>(this Action<T> action, T val)
    {
        if (action != null)
        {
            action(val);
        }
    }

    public static void SafeInvoke<T, U>(this Action<T, U> action, T val, U val2)
    {
        if (action != null)
        {
            action(val, val2);
        }
    }

    public static void SafeInvoke<T, U, S>(this Action<T, U, S> action, T val, U val2, S val3)
    {
        if (action != null)
        {
            action(val, val2, val3);
        }
    }
}
