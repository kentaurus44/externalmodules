﻿public static class StringExtensions
{
    public static bool IsEmptyOrNull(this string str)
    {
        return string.IsNullOrEmpty(str);
    }
}
