﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomTime
{
    private static float _timeScale = 1f;

    public static float TimeScale
    {
        get { return _timeScale; }
        set { _timeScale = value; }
    }

    public static float deltaTime
    {
        get { return _timeScale * Time.deltaTime; }
    }

    public static float fixedDeltaTime
    {
        get { return _timeScale * Time.fixedDeltaTime; }
    }
}
