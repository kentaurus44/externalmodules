﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> where T : MonoBehaviour
{
    private List<T> _pool = new List<T>();
    private List<T> _active = new List<T>();
    private Transform _parent;
    private T _original;

    public List<T> Active
    {
        get { return _active; }
    }

    public ObjectPool(T original, Transform parent, int autoLoad)
    {
        _original = original;
        _original.gameObject.SetActive(false);
        _parent = parent;

        T item = null;
        for (int i = 0; i < autoLoad; ++i)
        {
            item = GameObject.Instantiate<T>(_original, _parent);
            _pool.Add(item);
        }
    }

    public T GetItem()
    {
        T item = null;
        if (_pool.Count == 0)
        {
            item = GameObject.Instantiate<T>(_original, _parent);
        }
        else
        {
            item = _pool[0];
            _pool.RemoveAt(0);
        }

        item.gameObject.SetActive(true);
        _active.Add(item);

        return item;
    }

    public void ReleaseAll()
    {
        for (int i = _active.Count - 1; i >= 0; --i)
        {
            Release(_active[i]);
        }
    }

    public void Release(T item)
    {
        item.gameObject.SetActive(false);
        _pool.Insert(0, item);
        _active.Remove(item);
    }
	
}
