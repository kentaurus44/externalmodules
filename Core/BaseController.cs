﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.Singleton
{
	public class BaseController : MonoBehaviour
	{
		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		protected virtual void Awake()
		{

		}

		/// <summary>
		/// This function is called when the MonoBehaviour will be destroyed.
		/// </summary>
		protected virtual void OnDestroy()
		{

		}

		protected virtual void Init()
		{

		}

		protected virtual void Reset()
		{

		}
	}
}
