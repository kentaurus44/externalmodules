﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagersManager : MonoBehaviour
{
    [SerializeField]
    protected SingletonComponent[] _managers;

    public virtual IEnumerator Load()
    {
        SingletonComponent component;
        for (int i = 0; i < _managers.Length; ++i)
        {
            yield return component = Instantiate<SingletonComponent>(_managers[i], transform);
            component.name = _managers[i].name;
			OnSingletonLoaded(_managers[i].name);
			_managers[i] = null;
		}
        yield return null;
		_managers = null;
	}

	protected virtual void OnSingletonLoaded(string id)
	{

	}
}
