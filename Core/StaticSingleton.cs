﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace Core.Singleton
{
	public abstract class StaticSingleton<T> where T : new()
	{
		public static event Action OnInitialize;
		public static event Action OnReset;

		private static T _instance = default(T);

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new T();
				}
				return _instance;
			}
		}

		public StaticSingleton() { }

		public static bool IsInstanceNull { get { return _instance == null; } }

		public virtual void Init()
		{
			OnInitialize.SafeInvoke();
		}

		public virtual void Reset()
		{
			OnReset.SafeInvoke();
		}
	}
}