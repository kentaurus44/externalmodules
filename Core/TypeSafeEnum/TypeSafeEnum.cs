﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Linq;
using System;

#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(TypeSafeEnum))]
public class TypeSafeEnumDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        //EditorGUI.BeginProperty(position, label, property);

        //string value = property.stringValue;

        //ChapterEnum[] chapterEnum = ChapterEnum.GetArray();


        //List<string> list = new List<string>(chapterEnum.Select(x => x.ToString()));
        //int currentIndex = list.IndexOf(value);

        //currentIndex = EditorGUI.Popup(position, currentIndex, list.ToArray());

        //property.stringValue = chapterEnum[currentIndex];

        //EditorGUI.EndProperty();
    }
}
#endif

[Serializable]
public abstract class TypeSafeEnum
{
    protected readonly string Name;

    public static implicit operator string(TypeSafeEnum @enum)
    {
        return @enum.Name;
    }

    public override string ToString()
    {
        return Name;
    }

    protected TypeSafeEnum() { }

    protected TypeSafeEnum(string typeSafeEnumName)
    {
        Name = typeSafeEnumName;
    }
}

[Serializable]
public class TypeSafeEnum<T> : TypeSafeEnum where T : TypeSafeEnum
{
    //protected static List<TypeSafeEnum> AllValues = new List<TypeSafeEnum>();

    protected TypeSafeEnum(string typeSafeEnumName) : base (typeSafeEnumName) { }

    public static IEnumerable GetAll()
    {
        var type = typeof(T);
        var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

        return fields.Select(info => info.GetValue(null)).OfType<T>().ToArray();
    }

    public static T[] GetArray()
    {
        var type = typeof(T);
        var fields = type.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.DeclaredOnly);

        return fields.Select(info => info.GetValue(null)).OfType<T>().ToArray();
    }

    //public static TypeSafeEnum FromString(string roleString)
    //{
    //    return AllValues.Single(r => String.Equals(r.Name, roleString, StringComparison.OrdinalIgnoreCase));
    //}
}
