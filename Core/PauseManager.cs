﻿using System;
using Core.Singleton;

namespace Core.Pause
{
	public class PauseManager : StaticSingleton<PauseManager>
	{
		public static Action<bool> OnPause;

		private bool _isPaused = false;

		public bool IsPaused
		{
			get { return _isPaused; }
		}

		public void Pause()
		{
			_isPaused = true;
			OnPause.SafeInvoke(_isPaused);
		}

		public void Unpause()
		{
			_isPaused = false;
			OnPause.SafeInvoke(_isPaused);
		}
	}
}