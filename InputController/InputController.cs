﻿//
// Script name: InputController
//
//
// Programmer: Kentaurus
//

using UnityEngine;
using System.Collections;

public enum ePlayer
{
    PLAYER_1 = 1,
    PLAYER_2,
    PLAYER_3,
    PLAYER_4
}

public class InputController : MonoBehaviour
{
    #region Variables
    /// <summary>
    /// {0} = Player, {1} = Button
    /// </summary>
    protected string BUTTONS_KEY= "Joystick{0}Button{1}";
    protected string DIRECTION_KEY = "Axis {0}";

    protected enum eDirections
    {
        LEFT_HORIZONTAL_JS = 1,
        LEFT_VERTOCAL_JS,
        RIGHT_HORIZONTAL_JS,
        RIGHT_VERTICAL_JS,
        DONT_KNOW1,
        DONT_KNOW2,
        DPAD_HORIZONTAL,
        DPAD_VERTICAL
    }

    protected enum eButtons
    {
        LEFT = 0,
        BOTTOM,
        RIGHT,
        TOP,
        LEFT_BUTTON,
        RIGHT_BUTTON,
        LEFT_TRIGGER,
        RIGHT_TRIGGER
    }

    [SerializeField] protected ePlayer m_PlayerID = ePlayer.PLAYER_1;

    #endregion

    #region Unity API
    #endregion

    #region Public Methods
    protected virtual void Update()
    {
        if (Input.GetKeyDown(GetKeyCode((int)eButtons.LEFT)))
        {
            LeftAction();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.RIGHT)))
        {
            RightAction();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.TOP)))
        {
            UpAction();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.BOTTOM)))
        {
            DownAction();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.RIGHT_BUTTON)))
        {
            RightButton();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.LEFT_BUTTON)))
        {
            LeftButton();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.LEFT_TRIGGER)))
        {
            LeftButton();
        }

        if (Input.GetKeyDown(GetKeyCode((int)eButtons.RIGHT_TRIGGER)))
        {
            LeftButton();
        }

        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            LeftDirectional(GetAngle(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")); 
        }
    }
    #endregion

    #region Protected Methods
    protected string GetAxis(eDirections direction)
    {
        return string.Format(DIRECTION_KEY, (int)direction);
    }

    protected KeyCode GetKeyCode(int index)
    {
        return EnumExtensions.ParseEnum<KeyCode>(GetKeyString(index));
    }

    protected string GetKeyString(int index)
    {
        return string.Format(BUTTONS_KEY, (int)m_PlayerID, index);
    }

    protected virtual void LeftButton()
    {
        
    }

    protected virtual void RightButton()
    {
        
    }

    protected virtual void LeftAction()
    {
        
    }

    protected virtual void RightAction()
    {
        
    }

    protected virtual void UpAction()
    {
        
    }

    protected virtual void DownAction()
    {
        
    }

    protected virtual void RightTrigger()
    {

    }

    protected virtual void LeftTrigger()
    {

    }

    protected virtual void LeftDirectional(float angle, float horizontal, float vertical)
    {

    }
    #endregion

    #region Private Methods
    private float GetAngle(float x, float y)
    {
        float angle = Mathf.Atan2(y, x);

        if (y < 0)
        {
            angle += 2f * Mathf.PI;
        }

        return angle * Mathf.Rad2Deg;
    }
    #endregion

}
