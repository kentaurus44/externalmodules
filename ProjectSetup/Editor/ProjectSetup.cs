﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class ProjectSetup
{
    [MenuItem("Kentaurus/Project Setup/Create Folders")]
    private static void CreateFolders()
    {
        AssetDatabase.CreateFolder("Assets", "Resources");
        AssetDatabase.CreateFolder("Assets/Resources", "Prefabs");
        AssetDatabase.CreateFolder("Assets/Resources", "Panels");
        AssetDatabase.CreateFolder("Assets/Resources", "Materials");
        AssetDatabase.CreateFolder("Assets/Resources", "Textures");
        AssetDatabase.CreateFolder("Assets/Resources", "Configurations");

        AssetDatabase.CreateFolder("Assets", "Scripts");
        AssetDatabase.CreateFolder("Assets/Scripts", "Panels");
        AssetDatabase.CreateFolder("Assets/Scripts", "Singletons");

        AssetDatabase.CreateFolder("Assets", "Scenes");
        AssetDatabase.CreateFolder("Assets/Scenes", "Panels");
    }
}
