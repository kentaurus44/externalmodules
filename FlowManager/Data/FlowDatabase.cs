﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[CreateAssetMenu(fileName = "FlowDatabase", menuName = "Database/FlowManager/FlowDatabase", order = 1)]
public class FlowDatabase : Database<FlowDatabase>
{
	public const string kOpen = "Open";
	public const string kClose = "Close";

	public static readonly string[] kFlowActions = new string[]
	{
		kOpen,
		kClose
	};

	[SerializeField]
	private List<FlowConfig> _configs = new List<FlowConfig>();

	public List<FlowConfig> Configs { get { return _configs; } }
	
	public string[] ViewNames
	{
		get
		{
			List<string> display = new List<string>();

			display.AddRange(_configs.Select(x => x.Id));

			return display.ToArray();
		}
	}

	public FlowConfig Get(string id)
	{
		FlowConfig view = null;
		FlowConfig tempView = null;
		for (int i = 0, count = _configs.Count; i < count; ++i)
		{
			tempView = _configs[i];
			if (tempView.Id == id)
			{
				view = tempView;
				break;
			}
		}
		return view;
	}

	public void AddFlowConfig(FlowConfig config)
	{
		_configs.Add(config);
	}
}
