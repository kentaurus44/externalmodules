﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "FlowConfig", menuName = "Config/FlowManager/FlowConfig", order = 1)]
public class FlowConfig : ScriptableObject
{
	[System.Serializable]
	private class ValidFlow
	{
		[SerializeField, FlowAttribute]
		private string _flow;
		[SerializeField, FlowAction]
		private string _action;

		public bool IsEqual(string id, string action)
		{
			return _flow.Equals(id) || _flow.Equals("None") && _action.Equals(action);
		}

	}

	[SerializeField]
	private bool _isPopup = false;

	[Header("Loading Screen")]
	[SerializeField]
	private bool _onExit = false;
	[SerializeField]
	private bool _onEnter = false;
	[Header("Flow")]
	[SerializeField]
	private ValidFlow[] _validFlow;

	public bool IsFlowValid(string view, string action)
	{
		foreach (var validFlow in _validFlow)
		{
			if (validFlow.IsEqual(view, action))
			{
				return true;
			}
		}
		return false;
	}

	public string Id { get { return name; } }
	public bool IsPopup { get { return _isPopup; } }
	public bool UseLoadingEnter { get { return _onEnter; } }
	public bool UseLoadingExit { get { return _onExit; } }
}
