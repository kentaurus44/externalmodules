﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Singleton;
using System;

namespace Core.Flow
{
	public class FlowManager : StaticSingleton<FlowManager>
	{
		public delegate void OnLoadSceneCB(FlowConfig config, Dictionary<string, object> param, Action onSceneLoaded, Action startClosingSequence, Action onViewClosed, Action onViewLoaded, Action onFocusGained, Action SceneLoadedComplete);
		public static OnLoadSceneCB OnLoadScene;
		public static event Action OnLaunch;
		public static event Action OnClose;
		public static event Action OnSceneLoaded;
		public static event Action OnViewClosed;
		public static event Action StartClosingSequence;
		public static event Action OnViewLoaded;
		public static event Action OnFocusGained;
		public static event Action SceneLoadedComplete;

		private bool _canTriggerNextAction = true;
		private FlowDatabase _database;
		private Stack<FlowConfig> _viewStack = new Stack<FlowConfig>();


		public override void Init()
		{
			// Do not call base
		}

		public void Init(FlowDatabase db)
		{
			_database = db;
		}

		public void Launch()
		{
			base.Init();
			TriggerAction(FlowDatabase.kOpen, "Main");
			OnLaunch.SafeInvoke();
		}

		public void TriggerAction(string action, string view = "None", Dictionary<string, object> param = null)
		{
			if (!_canTriggerNextAction)
			{
				return;
			}


			if (_viewStack.Count == 0 || _viewStack.Peek().IsFlowValid(view, action))
			{
				FlowConfig config = _database.Get(view);
				_canTriggerNextAction = false;
				switch (action)
				{
					case FlowDatabase.kOpen:
						Open(config, param);
						break;
					case FlowDatabase.kClose:
						Close();
						break;
				}
			}
			else
			{
				switch (action)
				{
					case FlowDatabase.kOpen:
						Debug.LogErrorFormat("Flow", "Flow invalid <color=blue>{0}</color> cant perform action <color=purple>{1}</color> to <color=blue>{2}</color>.", _viewStack.Peek().Id, action, view);
						break;
					case FlowDatabase.kClose:
						Debug.LogErrorFormat("Flow", "Flow invalid <color=blue>{0}</color> cant perform action <color=purple>{1}</color>.", _viewStack.Peek().Id, action);
						break;
					default:
						Debug.LogError("Flow", "Something went wrong.");
						break;
				}
			}
		}

		private void Open(FlowConfig config, Dictionary<string, object> param)
		{
			OnLoadScene(config,
			param,
			() =>
			{
				_viewStack.Push(config);
				OnSceneLoaded.SafeInvoke();
			},
			() =>
			{
				StartClosingSequence.SafeInvoke();
			},
			() =>
			{
				OnViewClosed.SafeInvoke();
			},
			() =>
			{
				OnViewLoaded.SafeInvoke();
			},
			() =>
			{
				OnFocusGained.SafeInvoke();
			},
			() =>
			{
				_canTriggerNextAction = true;
				SceneLoadedComplete.SafeInvoke();
			});
		}

		private void Close()
		{
			OnClose.SafeInvoke();
			_viewStack.Pop();
			_canTriggerNextAction = true;
		}
	}
}