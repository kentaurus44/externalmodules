﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Core.Singleton;
using System;
using UnityEngine.SceneManagement;

namespace Core.Flow
{
	public class FlowController : BaseController
	{
		[SerializeField]
		private FlowDatabase _database;
		[SerializeField]
		private Canvas _canvas;
		[SerializeField]
		private FlowLoadingScreen _loadingScreen;

		private Stack<View> _views = new Stack<View>();
		private View _currentView;
		private Coroutine _coroutine;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		protected override void Awake()
		{
			FlowManager.OnInitialize += Init;
			FlowManager.OnReset += Reset;
			FlowManager.OnLoadScene += OnLoad;
			FlowManager.OnClose += Close;
			FlowManager.OnLaunch += OnLaunch;

			FlowManager.Instance.Init(_database);
			_database = null;
		}

		/// <summary>
		/// This function is called when the MonoBehaviour will be destroyed.
		/// </summary>
		protected override void OnDestroy()
		{
			FlowManager.OnInitialize -= Init;
			FlowManager.OnReset -= Reset;
			FlowManager.OnLoadScene -= OnLoad;
			FlowManager.OnClose -= Close;
			FlowManager.OnLaunch -= OnLaunch;
		}

		protected void OnLaunch()
		{
			
		}

		protected void OnLoad(FlowConfig config , Dictionary<string, object> param, Action onSceneLoaded, Action startClosingSequence, Action onViewClosed, Action onViewLoaded, Action onFocusGained, Action sceneLoadedComplete)
		{
			if (_coroutine == null)
			{
				_coroutine = StartCoroutine(SceneLoading(config, param, onSceneLoaded, startClosingSequence, onViewClosed, onViewLoaded, onFocusGained, sceneLoadedComplete));
			}
		}

		private IEnumerator SceneLoading(FlowConfig scene, Dictionary<string, object> param, Action onSceneLoaded, Action startClosingSequence, Action onViewClosed, Action onViewLoaded, Action onFocusGained, Action sceneLoadedComplete)
		{
			string id = scene.Id;

			if (_loadingScreen != null)
			{
				_loadingScreen.transform.SetAsLastSibling();

				if (!scene.IsPopup && scene.UseLoadingEnter || scene.UseLoadingExit)
				{
					_loadingScreen.Begin();
				}
			}

			yield return SceneManager.LoadSceneAsync(id, LoadSceneMode.Additive);
			View[] views;
			yield return views = FindObjectsOfType<View>();

			for (int i = 0, count = views.Length; i < count; ++i)
			{
				View view = views[i];

				if (id == view.name && view != _currentView && !_views.Contains(view))
				{
					onSceneLoaded.SafeInvoke();
					_currentView = view;
					break;
				}
			}

			_currentView.transform.SetParent(_canvas.transform, false);

			if (_loadingScreen != null)
			{
				_currentView.transform.SetSiblingIndex(_loadingScreen.transform.GetSiblingIndex() - 1);
			}

			yield return SceneManager.UnloadSceneAsync(id);

			if (!scene.IsPopup)
			{
				View view;
				while (_views.Count > 0)
				{
					view = _views.Pop();
					view.StartClosingSequence();
					yield return null;
					Destroy(view.gameObject);
				}
			}

			_views.Push(_currentView);

			onViewLoaded.SafeInvoke();
			_currentView.OnViewLoaded(param);

			if (_loadingScreen != null && _loadingScreen.isActiveAndEnabled)
			{
				while (!_currentView.LoadingComplete || _loadingScreen.IsTransitioning)
				{
					yield return null;
				}

				_loadingScreen.End();

				while (_loadingScreen.IsLoadingScreeenOpen)
				{
					yield return null;
				}
			}

			_currentView.OnFocusGain();
			onFocusGained.SafeInvoke();

			yield return null;

			_coroutine = null;

			sceneLoadedComplete.SafeInvoke();
		}

		private void Close()
		{
			Destroy(_currentView.gameObject);
			_currentView = _views.Pop();
		}
	}
}