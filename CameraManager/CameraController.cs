﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Core.CustomCamera
{
	public class CameraController : MonoBehaviour
	{
		[SerializeField]
		protected MainCameraController _mainCamera;
		[SerializeField]
		protected UICameraController _uiCamera;

		/// <summary>
		/// Awake is called when the script instance is being loaded.
		/// </summary>
		protected virtual void Awake()
		{
			CameraManager.OnInitialize += Init;
			CameraManager.OnReset += Reset;
		}

		/// <summary>
		/// This function is called when the MonoBehaviour will be destroyed.
		/// </summary>
		void OnDestroy()
		{
			CameraManager.OnInitialize -= Init;
			CameraManager.OnReset -= Reset;
		}

		private void Init()
		{
			_mainCamera.Init();
			_uiCamera.Init();
		}

		private void Reset()
		{

		}
	}
}
